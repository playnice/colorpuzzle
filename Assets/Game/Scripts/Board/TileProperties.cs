﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayNice.ColorPuzzle
{
	[CreateAssetMenu(fileName = "New Tile Property", menuName = "Color Puzzle/Tile Property", order = 0)]
	public class TileProperties : ScriptableObject, ITileProperties
	{
		[SerializeField] Sprite sprite;
		[SerializeField] Color color = Color.white;

		public Sprite Sprite => sprite;
		public Color Color => color;
		public string Name => name;
	}
}
