﻿using System;
using UnityEngine;
using UnityEngine.UI;
using PlayNice.Global.UI;
using PlayNice.Global;
using PlayNice.Global.PropertyDrawers;

namespace PlayNice.ColorPuzzle
{
	public interface IGameBoard
    {
		int Rows { get; }
		int Columns { get; }
		Action<Tile[,]> OnPopulate { get; set; }
		void Populate(float pCellSize, int pRows, int pColumns);
		void Show(bool pShow);
    }

	public class GameBoard : UIElement, IGameBoard
	{
		[SerializeField] Tile tilePrefab;
		[SerializeField] GridLayoutGroup grid;
		Tile[,] tiles = new Tile[0,0];
		public Action<Tile[,]> OnPopulate { get; set; }

		public void Populate (float pCellSize, int pRows, int pColumns)
		{
			Clear();
			grid.cellSize = new Vector2(pCellSize, pCellSize);
			grid.constraint = GridLayoutGroup.Constraint.FixedRowCount;
			grid.constraintCount = pRows;
			tiles = new Tile[pRows, pColumns];
			for (int i=0;i<pRows;i++)
			{
				for(int j=0;j<pColumns;j++)
				{
					Tile tile = (Tile)Instantiate(tilePrefab, grid.transform);
					tile.name = string.Format("Tile ({0}, {1})", i, j);
					tiles[i,j] = tile;
				}
			}

			if (OnPopulate != null)
				OnPopulate(tiles);
		}

		public void Clear()
		{
			// TODO: Object pool...
			grid.transform.RemoveChildren();
			tiles = new Tile[0, 0];
		}

		[ReadOnlyProperty]
		public int Rows => tiles.GetLength(0);
		[ReadOnlyProperty]
		public int Columns => tiles.GetLength(1);

		public Tile[,] Tiles => tiles;
	}
}
