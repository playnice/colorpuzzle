﻿using System;
using UnityEngine;
using UnityEngine.UI;
using PlayNice.Global;

namespace PlayNice.ColorPuzzle
{
	public class Tile : MonoBehaviour
	{
		[SerializeField] Image graphic;
		[SerializeField] Button button;

		public event Action<Tile> OnSelect;

		private void Start()
		{
			button.RegisterClickEvent(OnButtonClicked);
		}

		public void SetProperties (ITileProperties pProperties)
		{
			if(graphic)
			{
				graphic.sprite = pProperties.Sprite;
				graphic.color = pProperties.Color;
			}
			else
			{
				Debug.LogError(string.Format("[{0}]: Failed to set the tile <color=red>{1}</color>'s properties, its graphic component was null!", this.GetType().Name, name));
			}
		}

		void OnButtonClicked()
		{
			OnSelect?.Invoke(this);
		}
	}
}
