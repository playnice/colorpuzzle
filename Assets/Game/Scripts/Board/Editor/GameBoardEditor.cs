using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using PlayNice.Global;
using PlayNice.Global.UI;
using PlayNice.Global.PropertyDrawers;

namespace PlayNice.ColorPuzzle
{
	/// <summary>
	/// Draws a custom inspector window for the component of type GameBoard
	/// </summary>
	[CustomEditor(typeof(GameBoard), true)]
	public class GameBoardEditor : UIElementEditor
	{
		/// <summary>
		/// The current object of type GameBoard that is being drawn to the inspector window.
		/// </summary>
		GameBoard gameBoard;
		float cellSize = 150f;

		Vector2Int tableSpecifications = new Vector2Int(4, 4);

		/// <summary>
		/// Event fired when the inspector drawing this component begins
		/// </summary>
		protected override void OnEnable ()
		{
			base.OnEnable();
			gameBoard = target as GameBoard;
		}

		/// <summary>
		/// Fired once per frame while this component is actively being drawn to the inspector
		/// </summary>
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			if (Application.isPlaying)
				CustomEditorTools.DrawContents("Utilities", DrawUtilities);
			else
				EditorGUILayout.HelpBox("Testing utilities available at runtime!", MessageType.Info);
		}

		/// <summary>
		/// Draws custom developer utilities designed for testing, debugging, or setting up 
		/// the GameBoard instance that is actively being drawn to the inspector window.
		/// </summary>
		void DrawUtilities()
		{
			cellSize = EditorGUILayout.FloatField("Cell Size", cellSize);
			tableSpecifications = EditorGUILayout.Vector2IntField("Table Rows/Columns", tableSpecifications);
			
			if(GUILayout.Button("Populate"))
			{
				gameBoard.Populate(cellSize, tableSpecifications.x, tableSpecifications.y);
			}
		}
	}
}