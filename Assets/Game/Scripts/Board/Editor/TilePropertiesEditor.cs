using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using PlayNice.Global;

namespace PlayNice.ColorPuzzle
{
	/// <summary>
	/// Draws a custom inspector window for the component of type TileProperties
	/// </summary>
	[CustomEditor(typeof(TileProperties), true)]
	public class TilePropertiesEditor : Editor
	{
		/// <summary>
		/// The current object of type TileProperties that is being drawn to the inspector window.
		/// </summary>
		TileProperties tileProperties;

		/// <summary>
		/// Event fired when the inspector drawing this component begins
		/// </summary>
		void OnEnable ()
		{
			tileProperties = target as TileProperties;
		}

		/// <summary>
		/// Fired once per frame while this component is actively being drawn to the inspector
		/// </summary>
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			if (tileProperties.Sprite) CustomEditorTools.DrawContents("Preview", DrawPreview);
		}

		void DrawPreview ()
		{
			Texture texture = AssetPreview.GetAssetPreview(tileProperties.Sprite);
			GUILayout.Label(texture);
		}
	}
}