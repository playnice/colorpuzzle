﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayNice.Global;

namespace PlayNice.ColorPuzzle
{
    [CreateAssetMenu(fileName = "Tile Property Collection", menuName = "Color Puzzle/Tile Property Collection", order = 0)]
    public class TilePropertyCollection : ScriptableCollection<TileProperties>, ITileCollection
    {
        [SerializeField] Sprite preview;

        public string Name => name;
        public Sprite Preview => preview;
        public IList<ITileProperties> Tiles => Items.ConvertAllByCast<TileProperties, ITileProperties>();
    }
}
