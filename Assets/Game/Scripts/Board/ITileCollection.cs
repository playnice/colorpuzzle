﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayNice.ColorPuzzle
{
    public interface ITileCollection
    {
        string Name { get; }
        Sprite Preview { get; }
        IList<ITileProperties> Tiles { get; }
    }
}
