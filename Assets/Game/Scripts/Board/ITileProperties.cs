﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayNice.ColorPuzzle
{
	public interface ITileProperties
	{
		Color Color { get; }
		Sprite Sprite { get; }
		string Name { get; }
	}
}
