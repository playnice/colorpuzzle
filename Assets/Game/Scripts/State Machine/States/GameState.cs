﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayNice.ColorPuzzle
{
    [CreateAssetMenu(fileName = "Game", menuName = "Color Puzzle/States/Game", order = 0)]
    public class GameState : PuzzleState
    {
        IGameBoard Board => Ui?.Board;
        IGameHUD HUD => Ui?.GameHUD;

        public override void Activate(bool pActivate)
        {
            base.Activate(pActivate);
            if (Board != null)
                Board.Show(pActivate);

            if (HUD != null)
                HUD.Show(pActivate);
        }
    }
}
