﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayNice.Global;
using PlayNice.Global.UI;

namespace PlayNice.ColorPuzzle
{
	[CreateAssetMenu(fileName = "Level Editor", menuName = "Color Puzzle/States/Level Editor")]
	public class LevelEditorState : PuzzleState
	{
		IGameBoard Board => Ui?.Board;
		ILevelEditorUI EditorUI => Ui.EditorUI;
		TileSelectionMenu TileMenu => EditorUI?.TileMenu;
		CollectionMenu CollectionMenu => EditorUI?.CollectionMenu;
		[SerializeField] TilePropertyCollection tileCollection;
		List<Tile> selectedTiles = new List<Tile>();
		[SerializeField] List<TilePropertyCollection> collections = new List<TilePropertyCollection>();

		// Start is called before the first frame update
		void RegisterEvents (bool pRegister)
		{
			if(pRegister)
            {
				RegisterEvents(false);
				Board.OnPopulate += OnBoardPopulated;
				TileMenu.OnSelect += OnTileSelected;
				CollectionMenu.PopulateMenu(collections.ConvertAllByCast<TilePropertyCollection, ITileCollection>(), OnCollectionSelected);
				Board.Populate(150f, 4, 4);
            }
			else
            {
				Board.OnPopulate -= OnBoardPopulated;
				TileMenu.OnSelect -= OnTileSelected;
			}
		}

        public override void Initialize(IPuzzleMachine pMachine)
        {
            base.Initialize(pMachine);
        }

        public override void Activate(bool pActivate)
        {
            base.Activate(pActivate);
			RegisterEvents(pActivate);
			if(EditorUI != null)
            {
				EditorUI.Show(pActivate);
            }

			if(Board != null)
            {
				Board.Show(pActivate);
            }
        }

        public override void StateUpdate()
        {
			base.StateUpdate();
		}

		void OnTileSelected(ITileProperties selection)
		{
			foreach (Tile t in selectedTiles)
			{
				t.SetProperties(selection);
			}

			ClearSelections();
		}

		void OnCollectionSelected(ITileCollection collection)
        {
			TileMenu.PopulateMenu(collection.Tiles, OnTileSelected);
        }

		void ClearSelections()
		{
			while(selectedTiles.Count > 0)
			{
				selectedTiles[0].GetComponent<TileEditorComponent>().Activate(false);
			}
		}

		void OnBoardPopulated(Tile[,] tiles)
		{
			for (int row = 0; row < Board.Rows; row++)
			{
				for (int column = 0; column < Board.Columns; column++)
				{
					TileEditorComponent editorComponent = tiles[row, column].gameObject.GetComponent<TileEditorComponent>(true);
					editorComponent.OnActivated += OnTileActivated;
				}
			}
		}

		void OnTileActivated (Tile pTile, bool pActivated)
		{
			if(pActivated)
			{
				if(!selectedTiles.Contains(pTile))
				{
					selectedTiles.Add(pTile);
				}
			}
			else
			{
				selectedTiles.Remove(pTile);
			}
		}
	}
}
