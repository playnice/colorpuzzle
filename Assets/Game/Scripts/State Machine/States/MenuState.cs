﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayNice.Global;

namespace PlayNice.ColorPuzzle
{
    [CreateAssetMenu(fileName = "Menu", menuName = "Color Puzzle/States/Menu", order = 0)]
    public class MenuState : PuzzleState
    {
        IStateMenu Menu => Ui?.MainMenu;
        [SerializeField] List<PuzzleState> states = new List<PuzzleState>();

        public override void Initialize(IPuzzleMachine pMachine)
        {
            base.Initialize(pMachine);
            for (int i = 0; i < states.Count; i++)
            {
                states[i] = states[i].GetSharedRuntimeInstance();
            }

            if (Menu != null)
            { 
                Menu.PopulateMenu(states);
            }
        }

        public override void Activate(bool pActivate)
        {
            base.Activate(pActivate);
            if(Menu != null)
            {
                Menu.Show(pActivate);
                Menu.OnSelect -= OnStateSelected;
                if(pActivate)
                {
                    Menu.OnSelect += OnStateSelected;
                }
            }
        }

        void OnStateSelected (PuzzleState pState)
        {
            if(machine != null)
                machine.LoadState(pState);
        }
    }
}
