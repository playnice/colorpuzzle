using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using PlayNice.Global;
using PlayNice.Global.PropertyDrawers;

namespace PlayNice.ColorPuzzle
{
	/// <summary>
	/// Draws a custom inspector window for the component of type PuzzleStateMachine
	/// </summary>
	[CustomEditor(typeof(PuzzleStateMachine), true)]
	public class PuzzleStateMachineEditor : CoreEditor
	{
		/// <summary>
		/// The current object of type PuzzleStateMachine that is being drawn to the inspector window.
		/// </summary>
		PuzzleStateMachine PuzzleStateMachine;

		/// <summary>
		/// Event fired when the inspector drawing this component begins
		/// </summary>
		protected override void OnEnable ()
		{
			base.OnEnable();
			PuzzleStateMachine = target as PuzzleStateMachine;
		}

		/// <summary>
		/// Fired once per frame while this component is actively being drawn to the inspector
		/// </summary>
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			CustomEditorTools.DrawContents ("Utilities", DrawUtilities);
		}
		/// <summary>
		/// Draws custom developer utilities designed for testing, debugging, or setting up 
		/// the PuzzleStateMachine instance that is actively being drawn to the inspector window.
		/// </summary>
		void DrawUtilities()
		{

		}

		PuzzleStateMachine Machine
		{
			get
			{
				return PuzzleStateMachine;
			}
		}
	}
}