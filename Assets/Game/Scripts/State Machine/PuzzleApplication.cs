using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using PlayNice.Global;
using PlayNice.Global.Components;
using PlayNice.Global.PropertyDrawers;

namespace PlayNice.ColorPuzzle
{
	public class PuzzleApplication : Singleton<PuzzleApplication>
	{
		[EnforceType(typeof(IPuzzleMachine))]
		[SerializeField] MonoBehaviour stateMachineBehaviour;
		[EnforceType(typeof(IAdditiveSceneLoader))]
		[SerializeField] MonoBehaviour sceneLoaderBehaviour;
		[Scene] [SerializeField] string uiScene;

		protected override void Awake()
		{
			base.Awake();

			Scene uiSceneFile = default(Scene);
			if (IsSceneAdditivelyLoaded(uiScene, out uiSceneFile))
			{
				SceneManager.UnloadSceneAsync(uiScene);
			}

			if (SceneLoader != null)
			{
				SceneLoader.LoadScene(uiScene, true, OnUISceneLoaded);
			}
		}

		/// <summary>
		/// Gets a value representing whether or not the scene with the name passed via param is 
		/// in the set of actively loaded scenes.
		/// </summary>
		/// <returns><c>true</c> if this instance is scene loaded the specified pSceneName pScene; otherwise, <c>false</c>.</returns>
		/// <param name="pSceneName">The name of the scene to check if it is loaded in the active scenes</param>
		/// <param name="pScene">The scene found in the search, if any</param>
		protected bool IsSceneAdditivelyLoaded(string pSceneName, out Scene pScene)
		{
			pScene = default(Scene);
			for (int i = 0; i < SceneManager.sceneCount; i++)
			{
				Scene s = SceneManager.GetSceneAt(i);
				if (s.name == pSceneName)
				{
					pScene = s;
					return true;
				}
			}

			return false;
		}

		void OnUISceneLoaded(Scene pScene, bool pLoaded)
		{
			if (pScene.name == uiScene && pLoaded)
			{
				IPuzzleUI uiRoot = pScene.GetInterface<IPuzzleUI>();
				if (uiRoot != null)
				{
					uiRoot.Initialize();
					if (StateMachine != null)
					{
						StateMachine.Initialize (uiRoot);
					}
				}
			}
		}

		public static IPuzzleMachine StateMachine
		{
			get
			{
				return Instance.stateMachineBehaviour as IPuzzleMachine;
			}
		}

		public static IAdditiveSceneLoader SceneLoader
		{
			get
			{
				return Instance.sceneLoaderBehaviour as IAdditiveSceneLoader;
			}
		}
	}
}
