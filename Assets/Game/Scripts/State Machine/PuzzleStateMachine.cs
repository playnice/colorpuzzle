using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayNice.Global;
using PlayNice.Global.Components;
using UnityEngine.SceneManagement;
using PlayNice.Global.PropertyDrawers;

namespace PlayNice.ColorPuzzle
{
	public interface IPuzzleMachine
	{
		void Initialize(IPuzzleUI pUI);
		void LoadState(PuzzleState pState);
		void Back();
		IAdditiveSceneLoader SceneLoader { get; }
		IPuzzleUI UIManager { get; }
	}

	public class PuzzleStateMachine : ScriptableBehaviourMachine<PuzzleState>, IPuzzleMachine
	{
		protected override void Awake() {}

		public void Initialize(IPuzzleUI pUIManager)
		{
			UIManager = pUIManager;
			base.Awake();
			LoadState(registeredStates[0]);
			Initialized = true;
		}

        protected override void Update()
        {
            base.Update();
			// TODO: Integrate a back button into the UI
			if(Input.GetKeyDown (KeyCode.Escape))
            {
				Back();
            }
        }

        protected override void InitializeState(PuzzleState pState)
		{
			base.InitializeState(pState);
			if (pState != null)
				pState.Initialize(this);
		}

		public override void Back()
		{
			base.Back();
		}

		public override void LoadState(PuzzleState pState)
		{
			base.LoadState(pState);
		}

		protected override void Activate(PuzzleState pState, bool pActivate)
		{
			base.Activate(pState, pActivate);
		}

		protected override ITransition GetTransition(PuzzleState pStateFrom, PuzzleState pStateTo)
		{
			return base.GetTransition(pStateFrom, pStateTo);
		}

		public override bool IsTransitioning
		{
			get
			{
				return base.IsTransitioning;
			}
		}

		public IAdditiveSceneLoader SceneLoader
		{
			get
			{
				return PuzzleApplication.SceneLoader;
			}
		}

		public IPuzzleUI UIManager { get; private set; }

		[ReadOnlyProperty]
		public bool Initialized { get; private set; }
	}
}
