using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PlayNice.Global;
using PlayNice.Global.UI;

namespace PlayNice.ColorPuzzle
{
    public interface IPuzzleUI
    {
        IGameBoard Board { get; }
        ILevelEditorUI EditorUI { get; }
        IStateMenu MainMenu { get; }
        IGameHUD GameHUD { get; }
        void Initialize();
    }

    public class PuzzleUI : MonoBehaviour, IPuzzleUI
    {
        [SerializeField] GameBoard board;
        public IGameBoard Board => board;
        [SerializeField] LevelEditorUI editorUI;
        public ILevelEditorUI EditorUI => editorUI;
        [SerializeField] StateMenu stateMenu;
        public IStateMenu MainMenu => stateMenu;
        [SerializeField] GameHUD gameHUD;
        public IGameHUD GameHUD => gameHUD;

        public void Initialize()
        {
            
        }
    }
}
