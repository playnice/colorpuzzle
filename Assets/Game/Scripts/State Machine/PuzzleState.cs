using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayNice.Global;
using PlayNice.Global.Components;

namespace PlayNice.ColorPuzzle
{
	public class PuzzleState : ScriptableState, IAppState
	{
		[SerializeField] protected string displayName;
		protected IPuzzleMachine machine;
		protected IPuzzleUI Ui => machine.UIManager;
		public string DisplayName => displayName;

		public virtual void Initialize (IPuzzleMachine pMachine)
		{
			machine = pMachine;
		}

		public override void Activate(bool pActivate)
		{
			base.Activate(pActivate);
		}

		public override void StateUpdate()
		{
			base.StateUpdate();
		}
	}
}
