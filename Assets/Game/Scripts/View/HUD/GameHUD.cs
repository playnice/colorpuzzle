﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PlayNice.Global;
using PlayNice.Global.UI;

namespace PlayNice.ColorPuzzle
{
    public interface IGameHUD
    {
        void Show(bool pShow);
    }

    public class GameHUD : UIElement, IGameHUD
    {

    }
}
