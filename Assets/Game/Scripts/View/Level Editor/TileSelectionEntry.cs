﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PlayNice.Global;
using PlayNice.Global.UI;

namespace PlayNice.ColorPuzzle
{
	public class TileSelectionEntry : GenericSelectionEntry<ITileProperties>, IPoolable
	{
		[SerializeField] Image image;
		[SerializeField] Text label;

		public override void Initialize(ITileProperties pData, GenericDelegate<object, ITileProperties> pOnSelect)
		{
			base.Initialize(pData, pOnSelect);
			if (label)
				label.text = pData.Name;
			if (image)
				image.sprite = pData.Sprite;
		}
	}
}
