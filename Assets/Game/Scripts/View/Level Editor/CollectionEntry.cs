﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PlayNice.Global;
using PlayNice.Global.UI;

namespace PlayNice.ColorPuzzle
{
    public class CollectionEntry : GenericSelectionEntry <ITileCollection>
    {
        [SerializeField] Image image;
        [SerializeField] Text label;

        public override void Initialize(ITileCollection pData, GenericDelegate<object, ITileCollection> pOnSelect)
        {
            base.Initialize(pData, pOnSelect);
            if (image)
                image.sprite = pData.Preview;
            if (label)
                label.text = pData.Name;
        }
    }
}
