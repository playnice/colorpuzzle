﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayNice.Global;
using PlayNice.Global.UI;

namespace PlayNice.ColorPuzzle
{
    public class LevelEditorControl : UIElement, IMenuControl
    {
        [SerializeField] string displayName = "Control Name";
        public string Name => displayName;
    }
}
