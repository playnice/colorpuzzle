﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PlayNice.Global;
using PlayNice.Global.UI;

namespace PlayNice.ColorPuzzle
{
    public interface IMenuControl
    {
        string Name { get; }
        void Show(bool pShow);
    }

    public class CycleMenu : UIElement
    {
        [SerializeField] Text controlNameLabel;
        public IMenuControl Current { get; private set; }
        List<IMenuControl> Controls = new List<IMenuControl>();
        [SerializeField] Transform controlRoot;

        private void Start()
        {
            if(controlRoot != null)
            {
                Controls = new List<IMenuControl>(controlRoot.gameObject.GetInterfacesInChildren<IMenuControl>());
                foreach (IMenuControl menuControl in Controls) {
                    menuControl.Show(false);
                }
                Reset();
            }
        }

        public void Activate (IMenuControl pControl)
        {
            if(Current != null)
            {
                Current.Show(false);
            }

            Current = pControl;
            if(Current != null)
            {
                Current.Show(true);
                if (controlNameLabel)
                    controlNameLabel.text = Current.Name;
            }
        }

        public void Next()
        {
            if(Controls.Count > 0)
                Activate(Controls.GetCyclicalNext(Current));
        }

        public void Previous()
        {
            if(Controls.Count > 0)
                Activate(Controls.GetCyclicalPrevious(Current));
        }

        public void Reset()
        {
            if(Controls.Count > 0)
                Activate(Controls[0]);
        }
    }
}
