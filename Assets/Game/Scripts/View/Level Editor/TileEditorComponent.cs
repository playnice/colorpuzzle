﻿using System;
using UnityEngine;
using PlayNice.Global;

namespace PlayNice.ColorPuzzle
{
	[RequireComponent(typeof(Tile))]
	public class TileEditorComponent : MonoBehaviour
	{
		Tile Target => GetComponent<Tile>();
		bool tileSelected = false;
		public event Action<Tile, bool> OnActivated;

		[SerializeField] UnityBoolEvent onActivateEvent;

		private void Start()
		{
			Target.OnSelect += OnTileSelected;
		}

		void OnTileSelected (Tile pTile)
		{
			Activate(!tileSelected);
		}

		public void Activate (bool pActivate)
		{
			tileSelected = pActivate;
			OnActivated?.Invoke(Target, tileSelected);
			onActivateEvent?.Invoke(tileSelected);
		}
	}
}
