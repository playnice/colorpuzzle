﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using PlayNice.Global;
using PlayNice.Global.UI;

namespace PlayNice.ColorPuzzle
{
    [System.Serializable]
    public class CollectionSelectedEvent : UnityEvent<IList<ITileProperties>> { }

    public class CollectionMenu : GenericSelectionMenu <ITileCollection, CollectionEntry>
    {
        [SerializeField] CollectionSelectedEvent onEntrySelected;

        protected override void OnEntrySelected(CollectionEntry pEntry, ITileCollection pData)
        {
            base.OnEntrySelected(pEntry, pData);
            onEntrySelected?.Invoke(pData.Tiles);
        }
    }
}
