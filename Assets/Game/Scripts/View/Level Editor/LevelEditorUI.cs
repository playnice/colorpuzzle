﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayNice.Global.UI;

namespace PlayNice.ColorPuzzle
{
    public interface ILevelEditorUI
    {
        TileSelectionMenu TileMenu { get; }
        CollectionMenu CollectionMenu { get; }
        void Show(bool pShow);
    }

    public class LevelEditorUI : UIElement, ILevelEditorUI
    {
        [SerializeField] TileSelectionMenu tileMenu;
        public TileSelectionMenu TileMenu => tileMenu;
        [SerializeField] CollectionMenu collectionMenu;
        public CollectionMenu CollectionMenu => collectionMenu;       
    }
}
