﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PlayNice.Global.UI;
using PlayNice.Global.Components;
using PlayNice.Global;

namespace PlayNice.ColorPuzzle
{
    public class StateMenuButton : GenericSelectionEntry <PuzzleState>
    {
        [SerializeField] Text label;

        public override void Initialize(PuzzleState pData, GenericDelegate<object, PuzzleState> pOnSelect)
        {
            base.Initialize(pData, pOnSelect);
            label.text = pData.DisplayName;
        }
    }
}
