﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PlayNice.Global;
using PlayNice.Global.Components;
using PlayNice.Global.UI;

namespace PlayNice.ColorPuzzle
{
    public interface IStateMenu
    {
        Action<PuzzleState> OnSelect { get; set; }
        void PopulateMenu(IList<PuzzleState> pStates);
        void Show(bool pShow);
        
    }

    public class StateMenu : GenericSelectionMenu <PuzzleState, StateMenuButton>, IStateMenu
    {

    }
}
