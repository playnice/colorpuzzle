# README #

### Color Puzzle ###

This is the Unity repository for the Thailand project Color Puzzle app David made in Game Maker Studio
 
### Unity Version: ###

* 2019.4.14f1

## Target(s) ##
Devices:

* iOS (iPhone & iPad)

* Android

Resolution:

* TBD

### How do I get set up? ###
1. Download **Unity 2019.4.140f1** from [Unity Downloads](https://unity3d.com/get-unity/download/archive)
2. Open **git bash** at your favorite repository location
3. Pull the repo down using the ``git clone https://username@bitbucket.org/playnice/colorpuzzle.git``
4. Navigate to the git repository of the project by typing ``cd colorpuzzle``
5. Get all submodules in the project by typing ``git submodule init`` then in the ``./.git/config`` file under ``[submodule "Assets/Global"]`` edit the url to be ``https://username@bitbucket.org/playnice/playnice-global.git`` then ``git submodule update``.
6. Checkout the **master** branch by typing ``git checkout master``
7. Change to the **Assets/Global** location and checkout **master** from there
7. Open the **Unity** and open the project at the location you saved the folder **colorpuzzle**.
8. Once in Unity, use the **Library** to navigate to ``Assets/Game/Scenes/Main``
9. Set the game aspect to **TBD** with fixed resolution **1125x2436** 
10. Press the **Play** button and see it run!

### Contribution guidelines ###
* All tasks for this project are tracked on [Trello](https://trello.com/b/WO0Qmp3c/color-puzzle)
* Player settings are set for the C# architecture .NET 4.6 (Experimental)
* All assets/project files made specifically for the fitness app belong in ``Assets/Game`` nested to an appropriate folder for the content type.
* All assets/scripts that are PlayNice wide/project independent belong in ``Assets/Global``

### Who do I talk to? ###

* Alex Hollums - ahollums@playnice.games - 678 633 9385
* David Houle - jh04579@georgiasouthern.edu